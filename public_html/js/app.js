var app = angular.module('personalmaps', ['ui.bootstrap', 'pascalprecht.translate'])
    .value('lang', lang);

app.config(['$translateProvider', function($translateProvider) {
    // add translation table
    $translateProvider.translations(translations);
}]);

app.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/list', {
        templateUrl: 'patrials/list.html',
        controller: 'PlacesListController'
    });
    $routeProvider.when('/add', {
        templateUrl: 'patrials/form.html',
        controller: 'PlacesFormController'
    });
    $routeProvider.when('/edit/:placeId', {
        templateUrl: 'patrials/form.html',
        controller: 'PlacesFormController'
    });
    $routeProvider.otherwise({
        redirectTo: '/list'
    });
}])

