<?php

class m131022_081507_create_places_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('pm_places', array(
            'id' => 'pk',
            'p_title' => 'string NOT NULL',
            'p_description' => 'string NOT NULL',
            'p_lng' => 'float',
            'p_lat' => 'float',
            'p_user_id' => 'integer',
        ));
	}

	public function down()
	{
        $this->dropTable('pm_places');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}