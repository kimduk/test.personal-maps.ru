<?php $this->pageTitle=Yii::app()->name; ?>
<div>
    <div class="js-fileapi-wrapper upload-btn" id="choose">
        <div class="upload-btn_txt">Choose files</div>
        <input type="file" name="files" multiple/>
    </div>
    <div id="images"></div>
</div>
<script>window.FileAPI = {staticPath: '/js/FileAPI/dist/'};</script>
<script src="/js/FileAPI/dist/FileAPI.min.js"></script>
<script>
    FileAPI.event.on(choose, 'change', function (evt){
        var files = FileAPI.getFiles(evt); // Retrieve file list

        FileAPI.filterFiles(files, function (file, info/**Object*/){
            if( /^image/.test(file.type) ){
                return  info.width >= 320 && info.height >= 240;
            }
            return  false;
        }, function (files/**Array*/, rejected/**Array*/){
            if( files.length ){
                // Make preview 100x100
                FileAPI.each(files, function (file){
                    FileAPI.Image(file).preview(100).get(function (err, img){
                        images.appendChild(img);
                    });
                });

                // Uploading Files
                FileAPI.upload({
                    url: './ctrl.php',
                    files: { images: files },
                    progress: function (evt){ /* ... */ },
                    complete: function (err, xhr){ /* ... */ }
                });
            }
        });
    });
</script>

<a href='/images/blowball.jpg' class='fancy' rel='gallery'>
    <img src='/images/blowball.jpg' width='50'>
</a>


